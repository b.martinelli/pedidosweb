import axios from 'axios'

const getAll = (query = {}) => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(
        axios.get('https://g7kv8s1hr4.execute-api.us-east-1.amazonaws.com/pro/produtos')
          .then(response => {
            const data = response.data
            return data
          })

      )
    }, 500)
  })
}

const salvarProduto = (data) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      axios.post('https://g7kv8s1hr4.execute-api.us-east-1.amazonaws.com/pro/produtos', data)
    }, 500)
  })
}

export default { getAll, salvarProduto }
