import axios from 'axios'

const deletarProduto = (id) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      console.log('dado do delete ' + id)

      axios.delete('https://g7kv8s1hr4.execute-api.us-east-1.amazonaws.com/pro/produtos',
        { data: { id: JSON.stringify(id.id) } }
      )
    }, 500)
  })
}

export default { deletarProduto }
