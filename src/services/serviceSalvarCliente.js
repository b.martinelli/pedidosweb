import axios from 'axios'

const salvarCliente = (data) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      axios.post('https://g7kv8s1hr4.execute-api.us-east-1.amazonaws.com/pro/clientes', data)
    }, 500)
  })
}

export default { salvarCliente }
