import axios from 'axios'

const updateCliente = (data) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      console.log('id no update ' + data.id)
      axios.put('https://g7kv8s1hr4.execute-api.us-east-1.amazonaws.com/pro/clientes/', data)
    }, 500)
  })
}

export default { updateCliente }
