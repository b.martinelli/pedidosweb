import axios from 'axios'

const buscarCliente = (id) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      axios.get('https://g7kv8s1hr4.execute-api.us-east-1.amazonaws.com/pro/clientes/{id}', id)
        .then(response => {
          const data = response.data
          return data
        })
    }, 500)
  })
}

export default { buscarCliente }
