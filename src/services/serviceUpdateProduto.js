import axios from 'axios'

const updateProduto = (data) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      console.log('id no update ' + data.id)
      axios.put('https://g7kv8s1hr4.execute-api.us-east-1.amazonaws.com/pro/produtos/', data)
    }, 500)
  })
}

export default { updateProduto }
