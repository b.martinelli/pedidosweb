import React, { Component } from 'react'
import Nav from 'react-bootstrap/Nav'

export default class Header extends Component {
  render () {
    return (
      <div>
        <Nav defaultActiveKey='/' as='ul'>
          <Nav.Item as='li'>
            <Nav.Link href='/'>Pedidos</Nav.Link>
          </Nav.Item>
          <Nav.Item as='li'>
            <Nav.Link href='/clientes'>Clientes</Nav.Link>
          </Nav.Item>
          <Nav.Item as='li'>
            <Nav.Link href='/produtos'>Produtos</Nav.Link>
          </Nav.Item>
        </Nav>

      </div>
    )
  }
}
