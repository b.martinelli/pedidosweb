import React, { Component } from 'react'
import { Form, Row, Col, Button } from 'react-bootstrap'
import salvarProduto from '../../../services/serviceProdutos'
import { Link } from 'react-router-dom'
import updateProduto from '../../../services/serviceUpdateProduto'
export default class novoProduto extends Component {
    state = {
      id: null,
      nome: '',
      descricao: '',
      preco: 0,
      estoque: 0,
      status: '',
      campoPreco: ''
    }

      salvar = async () => {
        console.log('estou no async state ' + this.state.id)

        if (this.props.content) {
          this.setState({
            id: this.props.content.id

          })
          console.log('O Estado ' + this.state.id)
          await updateProduto.updateProduto(this.state)
        }
        await salvarProduto.salvarProduto(this.state)
      }
      componentDidMount () {
        if (this.props.content) {
          this.setState({
            id: this.props.content.id,
            nome: this.props.content.nome,
            cpf: this.props.content.cpf,
            email: this.props.content.email,
            fone: this.props.content.fone,
            endereco: this.props.content.endereco,
            cep: this.props.content.cep,
            cidade: this.props.content.cidade,
            uf: this.props.content.uf,
            status: this.props.content.status

          })
        }
      }

      render () {
        return (
          <div>
            <Form>
              <Form.Group as={Row} controlId='formHorizontalEmail'>
                <Form.Label column sm={1}>
                   Nome
                </Form.Label>
                <Col sm={6}>
                  <Form.Control value={this.state.nome} onChange={(event) => this.setState({ nome: event.target.value })} placeholder='Nome' />
                </Col>
              </Form.Group>
              <Form.Group as={Row} controlId='formHorizontalEmail'>
                <Form.Label column sm={1}>
                descricao
                </Form.Label>
                <Col sm={6}>
                  <Form.Control value={this.state.descricao} onChange={(event) => this.setState({ descricao: event.target.value })} type='text' placeholder='descricao' />
                </Col>
              </Form.Group>
              <Form.Group as={Row} controlId='formHorizontalEmail'>
                <Form.Label column sm={1}>
                preco
                </Form.Label>
                <Col sm={6}>
                  <Form.Control value={this.state.preco} onChange={(event) => this.setState({ preco: Number(event.target.value) })} type='text' placeholder='preco' />
                </Col>
              </Form.Group>

              <Form.Group as={Row} controlId='formHorizontalEmail'>
                <Form.Label column sm={1}>
                estoque
                </Form.Label>
                <Col sm={6}>
                  <Form.Control value={this.state.estoque}
                    onChange={(event) => this.setState({

                      estoque: Number(event.target.value)
                    })} type='text' placeholder='estoque' />
                </Col>
              </Form.Group>
              <Form.Group as={Row} controlId='formHorizontalEmail'>
                <Form.Label column sm={1}>
                status
                </Form.Label>
                <Col sm={6}>
                  <Form.Control value={this.state.status} onChange={(event) => this.setState({ status: event.target.value })} type='text' placeholder='status' />
                </Col>
              </Form.Group>
            </Form>

            <Link to='/produtos'> <Button onClick={this.salvar} >Salvar</Button> </Link>
          </div>
        )
      }
}
