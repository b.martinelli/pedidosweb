import React, { Component } from 'react'
import getAll from '../../services/serviceProdutos'
import { Table, Form, Col, Row, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

import { FiTrash } from 'react-icons/fi'
import { MdAssignment } from 'react-icons/md'

import deletarProduto from '../../services/serviceDeletarProduto'
import NovoProduto from './novoProduto'

export default class Produtos extends Component {
  state = {
    loading: true,
    produtos: [],
    produtoSelecionado: null,
    novo: true,
    delete: null
  }

  async componentDidMount () {
    this.setState({
      loading: false,
      produtos: await getAll.getAll()
    })
  }
  produtoSelecionado = (produto) => {
    this.setState({
      produtoSelecionado: produto
    })
    console.log(produto.id)
  }

  novo = () => {
    this.setState({
      novo: false
    })
  }
  async deletar (produto) {
    await deletarProduto.deletarProduto(produto)
    this.setState({
      loading: false,
      produtos: await getAll.getAll()
    })
  }
  render () {
    const produtos = <Table striped bordered hover>
      <thead>
        <tr>
          <th>Id</th>
          <th>Nome</th>
          <th>Preco</th>
          <th>Estoque</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>

        {this.state.produtos.map((produto) => (
          <tr key={produto.id}>
            <td>{produto.id}</td>
            <td>{produto.nome}</td>
            <td>{produto.preco}</td>
            <td>{produto.estoque}</td>
            <td>{produto.status}</td>
            <td>
              <MdAssignment onClick={() => { this.produtoSelecionado(produto) }} />
              <FiTrash onClick={() => { this.deletar(produto) }} />

            </td>
          </tr>
        ))}

      </tbody>
    </Table>
    const novoProduto = <NovoProduto />
    const updateProduto = <NovoProduto content={this.state.produtoSelecionado} />
    return (
      <div>
        {this.state.loading && (
          <p>Carregando Produtos</p>
        )}

        {!this.state.loading && (
          <div>
            <div>
              <Form>
                <Form.Group as={Row} controlId='formHorizontalEmail'>
                  <Form.Label column sm={1}>
                     Produto
                  </Form.Label>
                  <Col sm={4}>
                    <Form.Control type='email' placeholder='digite o nome do produto' />
                  </Col>
                  <Col>
                    <Button> Procurar</Button>
                    <Link to='/novoProduto'><Button onClick={() => this.novo()}>Novo</Button></Link>

                  </Col>

                </Form.Group>
              </Form>

              { this.state.novo ? produtos : novoProduto }
              { this.state.produtoSelecionado !== null ? updateProduto : null}

            </div>
          </div>
        )}
      </div>
    )
  }
}
