import React, { Component } from 'react'
import * as servicePedidos from '../../services/servicePedidos'
import { Table } from 'react-bootstrap'
export default class Pedidos extends Component {
  state = {
    loading: true,
    pedidos: [],
    pedidoSelecionado: null,
    novo: true,
    delete: null
  }

  async componentDidMount () {
    this.setState({
      loading: false,
      pedidos: await servicePedidos.getAll()
    })
    console.log(this.state.pedidos)
  }

  clienteSelecionado = (pedido) => {
    this.setState({
      pedidoSelecionado: pedido
    })
    console.log(pedido.id)
  }

  novo = () => {
    this.setState({
      novo: false
    })
  }

  render () {
    const pedidos = <Table striped bordered hover>
      <thead>
        <tr>
          <th>Id</th>
          <th>Cliente</th>
          <th>Data</th>
          <th>Total</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>

        {this.state.pedidos.map((pedido) => (
          <tr key={pedido.id}>
            <td>{pedido.id}</td>
            <td>{pedido.cliente}</td>
            <td>{pedido.data}</td>
            <td>{pedido.total}</td>
            <td>{pedido.status}</td>
          </tr>
        ))}

      </tbody>
    </Table>
    return (
      <div>
        {this.state.loading && (
          <p>Carregando Pedidos</p>
        )}

        {!this.state.loading && (
          <div>
            { pedidos }
          </div>

        )}
      </div>
    )
  }
}
