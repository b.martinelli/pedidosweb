import React, { Component } from 'react'
import { Form, Row, Col, Button } from 'react-bootstrap'
import salvarCliente from '../../../services/serviceSalvarCliente'
import updateCliente from '../../../services/serviceUpdateCliente'
import { Link } from 'react-router-dom'

export default class novoCliente extends Component {
  state = {
    data: '',
    total: '',
    id_cliente: '',
    observacao: '',
    status: '',
    produtos: [
      {
        id_produto: '',
        quantidade: '',
        preco: ''
      }
    ]
  }
  salvar = async () => {
    console.log('estou no async state ' + this.state.id)

    if (this.props.content) {
      this.setState({
        id: this.props.content.id

      })
      console.log('O Estado ' + this.state.id)
      await updateCliente.updateCliente(this.state)
    }
    await salvarCliente.salvarCliente(this.state)
  }

  componentDidMount () {
    if (this.props.content) {
      this.setState({
        id: this.props.content.data,
        nome: this.props.content.total,
        cpf: this.props.content.id_cliente,
        email: this.props.content.observacao,
        fone: this.props.content.status,
        endereco: this.props.content.produtos

      })
    }
  }

  render () {
    return (
      <div>
        <Form>
          <Form.Group as={Row} controlId='formHorizontalEmail'>
            <Form.Label column sm={1}>
               Data
            </Form.Label>
            <Col sm={6}>
              <Form.Control value={this.state.data} onChange={(event) => this.setState({ data: event.target.value })} placeholder='Nome' />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId='formHorizontalEmail'>
            <Form.Label column sm={1}>
                Total
            </Form.Label>
            <Col sm={6}>
              <Form.Control value={this.state.total} onChange={(event) => this.setState({ total: event.target.value })} type='text' placeholder='Email' />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId='formHorizontalEmail'>
            <Form.Label column sm={1}>
                Id_Cliente
            </Form.Label>
            <Col sm={6}>
              <Form.Control value={this.state.id_cliente} onChange={(event) => this.setState({ id_cliente: event.target.value })} type='text' placeholder='Cpf' />
            </Col>
          </Form.Group>

          <Form.Group as={Row} controlId='formHorizontalEmail'>
            <Form.Label column sm={1}>
                Obs
            </Form.Label>
            <Col sm={6}>
              <Form.Control value={this.state.observacao} onChange={(event) => this.setState({ observacao: event.target.value })} type='text' placeholder='Fone' />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId='formHorizontalEmail'>
            <Form.Label column sm={1}>
               Status
            </Form.Label>
            <Col sm={6}>
              <Form.Control value={this.state.status} onChange={(event) => this.setState({ status: event.target.value })} type='text' placeholder='Endereco' />
            </Col>
          </Form.Group>
        </Form>

        <Link to='/clientes'>  <Button onClick={this.salvar}>Salvar</Button></Link>

      </div>
    )
  }
}
