import React, { Component } from 'react'
import * as serviceClientes from '../../services/serviceClientes'
import { Table, Form, Row, Col, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

import { FiTrash } from 'react-icons/fi'
import { MdAssignment } from 'react-icons/md'
import NovoCliente from '../Clientes/novoCliente'
import deletarCliente from '../../services/serviceDeletarCliente'

export default class Clientes extends Component {
  state = {
    loading: true,
    clientes: [],
    clienteSelecionado: null,
    novo: true,
    delete: null
  }

  async componentDidMount () {
    this.setState({
      loading: false,
      clientes: await serviceClientes.getAll()
    })
    console.log(this.state.clientes)
  }

  clienteSelecionado = (cliente) => {
    this.setState({
      clienteSelecionado: cliente
    })
    console.log(cliente.id)
  }

  novo = () => {
    this.setState({
      novo: false
    })
  }

  async deletar (cliente) {
    await deletarCliente.deletarCliente(cliente)
    this.setState({
      loading: false,
      clientes: await serviceClientes.getAll()
    })
  }

  render () {
    const clientes =
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Id</th>
            <th>Nome</th>
            <th>Email</th>
            <th>Fone</th>
            <th>Status  </th>
          </tr>
        </thead>
        <tbody>
          {this.state.clientes.map((cliente) => (
            <tr key={cliente.id}>
              <td>{cliente.id}</td>
              <td>{cliente.nome}</td>
              <td>{cliente.email}</td>
              <td>{cliente.fone}</td>
              <td>{cliente.status} </td>
              <td>
                <MdAssignment onClick={() => { this.clienteSelecionado(cliente) }} />
                <FiTrash onClick={() => { this.deletar(cliente) }} />

              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    const novoCliente = <NovoCliente />
    const updateCliente = <NovoCliente content={this.state.clienteSelecionado} />

    return (
      <div>

        {this.state.loading && (
          <p> Carregando Clientes</p>
        )}

        {!this.state.loading && (
          <div>

            <Form>
              <Form.Group as={Row} controlId='formHorizontalEmail'>
                <Form.Label column sm={1}>
                     Cliente
                </Form.Label>
                <Col sm={4}>
                  <Form.Control type='email' placeholder='digite o nome do cliente' />
                </Col>
                <Col>
                  <Button> Procurar</Button>
                  <Link to='/novoCliente'><Button onClick={() => this.novo()}>Novo</Button></Link>

                </Col>

              </Form.Group>
            </Form>

            { this.state.novo ? clientes : novoCliente }
            { this.state.clienteSelecionado !== null ? updateCliente : null}

          </div>
        )}
      </div>
    )
  }
}
