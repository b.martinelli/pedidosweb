import React, { Component } from 'react'
import { Form, Row, Col, Button } from 'react-bootstrap'
import salvarCliente from '../../../services/serviceSalvarCliente'
import updateCliente from '../../../services/serviceUpdateCliente'
import { Link } from 'react-router-dom'

export default class novoCliente extends Component {
  state = {
    id: null,
    nome: 'O',
    cpf: '',
    email: '',
    fone: '',
    endereco: '',
    cep: '',
    cidade: '',
    uf: '',
    status: '',
    updateCliente: []
  }
  salvar = async () => {
    console.log('estou no async state ' + this.state.id)

    if (this.props.content) {
      this.setState({
        id: this.props.content.id

      })
      console.log('O Estado ' + this.state.id)
      await updateCliente.updateCliente(this.state)
    }
    await salvarCliente.salvarCliente(this.state)
  }

  componentDidMount () {
    if (this.props.content) {
      this.setState({
        id: this.props.content.id,
        nome: this.props.content.nome,
        cpf: this.props.content.cpf,
        email: this.props.content.email,
        fone: this.props.content.fone,
        endereco: this.props.content.endereco,
        cep: this.props.content.cep,
        cidade: this.props.content.cidade,
        uf: this.props.content.uf,
        status: this.props.content.status

      })
    }
  }

  render () {
    return (
      <div>
        <Form>
          <Form.Group as={Row} controlId='formHorizontalEmail'>
            <Form.Label column sm={1}>
               Nome
            </Form.Label>
            <Col sm={6}>
              <Form.Control value={this.state.nome} onChange={(event) => this.setState({ nome: event.target.value })} placeholder='Nome' />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId='formHorizontalEmail'>
            <Form.Label column sm={1}>
                Email
            </Form.Label>
            <Col sm={6}>
              <Form.Control value={this.state.email} onChange={(event) => this.setState({ email: event.target.value })} type='text' placeholder='Email' />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId='formHorizontalEmail'>
            <Form.Label column sm={1}>
                Cpf
            </Form.Label>
            <Col sm={6}>
              <Form.Control value={this.state.cpf} onChange={(event) => this.setState({ cpf: event.target.value })} type='text' placeholder='Cpf' />
            </Col>
          </Form.Group>

          <Form.Group as={Row} controlId='formHorizontalEmail'>
            <Form.Label column sm={1}>
                Fone
            </Form.Label>
            <Col sm={6}>
              <Form.Control value={this.state.fone} onChange={(event) => this.setState({ fone: event.target.value })} type='text' placeholder='Fone' />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId='formHorizontalEmail'>
            <Form.Label column sm={1}>
                Endereco
            </Form.Label>
            <Col sm={6}>
              <Form.Control value={this.state.endereco} onChange={(event) => this.setState({ endereco: event.target.value })} type='text' placeholder='Endereco' />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId='formHorizontalEmail'>
            <Form.Label column sm={1}>
            Cep
            </Form.Label>
            <Col sm={6}>
              <Form.Control value={this.state.cep} onChange={(event) => this.setState({ cep: event.target.value })} type='text' placeholder='Cep' />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId='formHorizontalEmail'>
            <Form.Label column sm={1}>
                Cidade
            </Form.Label>
            <Col sm={6}>
              <Form.Control value={this.state.cidade} onChange={(event) => this.setState({ cidade: event.target.value })} type='text' placeholder='Cidade' />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId='formHorizontalEmail'>
            <Form.Label column sm={1}>
               Uf
            </Form.Label>
            <Col sm={6}>
              <Form.Control value={this.state.uf} onChange={(event) => this.setState({ uf: event.target.value })} type='text' placeholder='Uf' />
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId='formHorizontalEmail'>
            <Form.Label column sm={1}>
               Status
            </Form.Label>
            <Col sm={6}>
              <Form.Control value={this.state.status} onChange={(event) => this.setState({ status: event.target.value })} type='text' placeholder='Status' />
            </Col>
          </Form.Group>
        </Form>

        <Link to='/clientes'>  <Button onClick={this.salvar}>Salvar</Button></Link>

      </div>
    )
  }
}
