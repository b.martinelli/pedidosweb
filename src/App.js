import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Clientes from './pages/Clientes'
import Pedidos from './pages/Pedidos'
import Produtos from './pages/Produtos'
import novoCliente from './pages/Clientes/novoCliente'
import novoProduto from './pages/Produtos/novoProduto'
import Header from './components/Header'
import 'bootstrap/dist/css/bootstrap.css'

class App extends Component {
  render () {
    return (

      <BrowserRouter>
        <div>
          <Header />

          <Switch>
            <Route path='/' exact component={Pedidos} />
            <Route path='/clientes' component={Clientes} />
            <Route path='/produtos' component={Produtos} />
            <Route path='/novoCliente' component={novoCliente} />
            <Route path='/novoProduto' component={novoProduto} />

          </Switch>
        </div>
      </BrowserRouter>

    )
  }
}

export default App
